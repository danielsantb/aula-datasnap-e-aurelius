unit uClienteDAO;

interface

  uses
    uGenericDAO, uCliente;

  type
    TClienteDAO = class(TGenericDAO<TCliente>)
    end;

implementation

end.
