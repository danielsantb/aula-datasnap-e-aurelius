program DS3Camadas;

uses
  Vcl.Forms,
  uPrincipal in 'uPrincipal.pas' {Form4},
  uProxy in 'uProxy.pas',
  uBasicEntity in '..\Servidor\Models\uBasicEntity.pas',
  uCidade in '..\Servidor\Models\uCidade.pas',
  uCliente in '..\Servidor\Models\uCliente.pas',
  uEndereco in '..\Servidor\Models\uEndereco.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm4, Form4);
  Application.Run;
end.
